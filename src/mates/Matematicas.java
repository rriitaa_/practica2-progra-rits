/*Copyright [2022] [Rita Barragan]
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

/**
 * Esta clase contiene un metodo para generar el numero pi
 * @author Rita
 * @version 1.0 10/03/2022
 */

package mates;

public class Matematicas {

        private static double lanzarUnSoloDardo() {
                // Simplemente, si las coordenadas generadas aleatoriamente están dentro del
                // círculo, se devuelve 1 y, en otro caso, se devuelve 0.
                double x = Math.random();
                double y = Math.random();

                if ((x * x) + (y * y) < 1)
                        return 1;
                else
                        return 0;
        }

        /**
         * Genera el número pi usando el metodo recursivo, es decir que se nombra a sí
         * mismo
         *
         * @param pasos Es el numero de intentos para que llegue al numero pi
         * @return Devuelve una aproximación al número pi
         */
        private static double lanzarUnSoloDardoA(long pasos) {
                if (pasos == 1) {
                        return lanzarUnSoloDardo(); // ESTE ES MI PASO BASICO
                } else {
                        return lanzarUnSoloDardoA(pasos - 1) + lanzarUnSoloDardo();
                }
        }

        public static double generarAproximacionPi(long pasos) {

		if(pasos > 10000) throw new StackOverflowError("el numero que has escrito no va a ser valido");
		double lanzarUnSoloDardoA = generarAproximacionPi(pasos);
                return 4 * (float) generarAproximacionPi(pasos) / pasos;
        }
}


	//PRIMERO: PASO BASICO: tiras un dardo y ves si has acertado o no
	//tienes una diana, el centro es pi y si tiras dardos tienes q ver cual ha sido la aproximacion al centro 
	//el while de la practica 1, hay q cambiarlo a recursivo
	//recursivo es un metodo que se llama a si mismo, creando un bucle
	//hacemos recursivo(); para hacer la llamada
	//he creado lanzarUnSoloDardo y lanzarUnSoloDardoA
	//Al hacer java -jar n.jar n; n siendo un numero menor o igual a 3200, ya que si pruebas con numeros grandes te dara un overflow
